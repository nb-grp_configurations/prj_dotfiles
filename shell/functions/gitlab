#! /bin/env bash
gl-sq-jobs()
{
if [[ $# -eq 0 ]] ; then
  echo 'sidekiq current path and file missing'
  exit 0
fi
jq -cR 'fromjson?' $1 | jq -c 'select(.class != null) | .class'  |sort |uniq -c |sort -nr
}

gl-tw()
{
    curl -s 'https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml' | yq '.stages.[].groups.[] | select(.name | test("(?i)'"$*"'")) | {.name: [] + .tech_writer | join(", ")}'
}

gl-get-doc-path()
{
  echo "http -b --pretty all https://gitlab.com/gitlab-org/gitlab/-/tree/master/${1}"
}
gl-get-project-path()
{
PROJECT_HASH=$(echo -n $1 | openssl dgst -sha256 | sed 's/^.* //')
echo "/var/opt/gitlab/git-data/repositories/@hashed/${PROJECT_HASH:0:2}/${PROJECT_HASH:2:2}/${PROJECT_HASH}.git"
}

op-login ()
{
#one password cli
eval $(op signin gitlab)
}

op-get-vaults ()
{
op get vaults
}

rcli ()
{
sudo /opt/gitlab/embedded/bin/redis-cli -s /var/opt/gitlab/redis/redis.socket "$@";
}

DEL-gl-completely ()
{
rm -rf /opt/gitlab/
rm -rf /var/opt/gitlab/
rm -rf /var/log/gitlab/
rm -rf /etc/gitlab/
echo "delete postgresql data reminder"
}

gl-rake-check()
{
gitlab-rake gitlab:check
}

gl-rake-shell()
{
gitlab-rake gitlab_shell:check
}

gl-rake-gitaly()
{
gitlab-rake gitaly:check
}

gl-rake-sidekiq()
{
gitlab-rake sidekiq:check
}

gl-clear-redis()
{
gitlab-rake cache:clear
}

gl-status()
{
gitlab-ctl status
}

push-dotfiles(){
if [[ $# -eq 0 ]] ; then
  echo 'commit message missing'
  exit 1
fi
cd ~/gitrepos/dotfiles
#git add .
#echo "git commit -am \"${1}\""
git commit -am \"${1}\"
git push
}
##
#### API
##

gl-get-users ()
{
#if [[ $# -eq 0 ]] ; then
#echo " example cli:"
# defaults to localhost if no params
# given a gitlab.com private token in the .pat-gitlab-com file
# then gitlab.com may be queried in this fashion:
# "gl-get-users http://gitlab.com $(< ~/.pat-gitlab-com)"
#fi
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/users" "Private-Token: ${GLPAT}"
}

gl-get-projects ()
{
# example cli:
# $gl-get-users localhost $(< ~/.pat-gitlab-com)
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects" "Private-Token: ${GLPAT}"
}

gl-get-project-users ()
{
# example cli:
# gl-get-project-users endpoint token project_id
# gl-get-project-users localhost $(< ~/.pat) 0
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}/users" "Private-Token: ${GLPAT}"
}

gl-status-set ()
{
# example cli:
# gl-set-status endpoint token availability emoji message clear_status-after
# gl-set-status https://gitlab.com $(< ~/.pat) busy red_dot 8_hours
echo "gl-set-status http -b --pretty all https://gitlab.com $(< ~/.pat-gitlab-admiralboom) busy red_circle busy 1_day)"
#
#**For out of office:** `OOO`, `PTO`, `Parental Leave`, `Friends and Family`
#**For in office, but occupied:** `Training`, `Overburdened`, `Project Deliverable`, `IT Problems`
#**Emojis also work** palm_tree, beach, beach_umbrella, beach_with_umbrella,\
# ferris_wheel, thermometer, face_with_thermometer, red_circle, bulb, sun_with_face
#
GLENDPOINT=${1:-http://gitlab.com}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
echo "valid Avail 30_minutes, 3_hours, 8_hours, 1_day, 3_days, 7_days, 30_days"
AVAILABILITY=${3:-busy}
echo "valid emoji:  palm_tree, beach, beach_umbrella, beach_with_umbrella, ferris_wheel, thermometer, face_with_thermometer, red_circle, bulb, sun_with_face"
EMOJI=${4:-red_circle}
MESSAGE=${5:-busy}
CLEAR_STATUS_AFTER=${6:-1_day}
echo $AVAILABILITY $EMOJI $MESSAGE $CLEAR_STATUS_AFTER
http -b --pretty all -f PUT "${GLENDPOINT}/api/v4/user/status/" "Private-Token: ${GLPAT}" "availability=${AVAILABILITY}" "emoji=${EMOJI}" "message=${MESSAGE}" "clear_status_after=${CLEAR_STATUS_AFTER}"
}

gl-status-get ()
{
  # pull a users status
  # example: gl-get-status https://gitlab.com $(< ~/.pat-gitlab-admiralboom) user
#GLENDPOINT=${1:-https://gitlab.com}
#GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
USER=${1:-admiralboom}
http -b --pretty all "https://gitlab.com/api/v4/users/${USER}/status"
}

gl-status-clear()
{
GLENDPOINT=${1:-https://gitlab.com}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
# valid Avail 30_minutes, 3_hours, 8_hours, 1_day, 3_days, 7_days, 30_days
AVAILABILITY=${3:-}
# valid emoji:  palm_tree, beach, beach_umbrella, beach_with_umbrella, ferris_wheel, thermometer, face_with_thermometer, red_circle, bulb, sun_with_face
EMOJI=${4:-soccer}
MESSAGE=${5:-COYG}
CLEAR_STATUS_AFTER=${6:-1_day}
echo $AVAILABILITY $EMOJI $MESSAGE $CLEAR_STATUS_AFTER
http -b --pretty all -f PUT "${GLENDPOINT}/api/v4/user/status/" "Private-Token: ${GLPAT}" "availability=${AVAILABILITY}" "emoji=${EMOJI}" "message=${MESSAGE}" "clear_status_after=${CLEAR_STATUS_AFTER}"
}

gl-get-projects-by-id ()
{
# example cli:
# gl-get-project-users endpoint token project_id
# gl-get-project-users localhost $(< ~/.pat) 0
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}" "Private-Token: ${GLPAT}"
}

gl-get-groups ()
{
# example cli:
# gl-get-project-users localhost $(< ~/.pat) 0
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/groups" "Private-Token: ${GLPAT}"
}

gl-POST-groups ()
{
# example cli:
# gl-get-project-users localhost $(< ~/.pat) 0
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/groups" "Private-Token: ${GLPAT}"
}

gl-get-assets ()
{
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}/releases/${4}/assets/links" "Private-Token: ${GLPAT}"
}

gl-get-asset-link ()
{
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}/releases/${4}/assets/links/${5}" "Private-Token: ${GLPAT}"
}

gl-get-project-merge-request ()
{
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}/merge_requests" "Private-Token: ${GLPAT}"
}

gl-get-merge-request-by-ID ()
{
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}/merge_requests/:${4}" "Private-Token: ${GLPAT}"
}

gl-get-project-merge-request-state-var ()
{
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
if [[ ${4} != opened ]] && [[ ${4} != closed ]] && [[ ${4} != locked ]] && [[ ${4} != merged ]];
then
  echo "state can be one of opened closed locked or merged you entered ${4}"
  return 1
  else
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${3}/merge_requests?state=${4}" "Private-Token: ${GLPAT}"
fi
}

gl-get-pipeline-vars ()
{
# gl-get-pipeline-vars  "http -b --pretty alls://gitlab.com" $(< ~/.pat-gitlab-capn-clutch) # 16508302 148799228
GLENDPOINT=${1:-localhost}
GLPAT=${2:-$(< ~/.pat-gitlab-admiralboom)}
GLPROJECTID=${3:-16508302}
GLPIPELINEID=${4:-148799228}
http -b --pretty all "${GLENDPOINT}/api/v4/projects/${GLPROJECTID}/pipelines/${GLPIPELINEID}/variables" "Private-Token: ${GLPAT}"
}
