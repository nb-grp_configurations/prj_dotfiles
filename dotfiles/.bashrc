#! /bin/bash

# make the shells match
. /etc/profile

# set the shell explictly
SHELL=/bin/bash


# test if the prompt var is not set
if [ -z "$PS1" ]; then
    # prompt var is not set, so this is *not* an interactive shell
    return
fi

# load aliases in all shells
#
. ~/.dotfiles/shell/aliases
. ~/.dotfiles/shell/env

# check for stub file presence and load functions to build environment
#
[[ -e ~/.ws ]] &&
for file in ~/.dotfiles/shell/functions/*;
  do source "${file}";
  echo loading "${file}";
done;

# setup PS1 with git branch, system loads and clock
#
. ~/scripts/prmpt

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export SSH_AUTH_SOCK=~/.ssh/ssh-agent.$HOSTNAME.sock
ssh-add -l 2>/dev/null >/dev/null
#if [ $? -ge 2 ]; then
#  ssh-agent -a "$SSH_AUTH_SOCK" >/dev/null
#fi

# cpan additions
PATH="/home/boom/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/boom/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/boom/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/boom/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/boom/perl5"; export PERL_MM_OPT;
