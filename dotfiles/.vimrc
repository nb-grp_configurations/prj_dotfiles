" let $RC="$HOME/.vim/vimrc"
" let $RTP=split(&runtimepath, ',')[0]
set nocompatible              " be iMproved, required
" set path=.,**

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
" mkdir ~/.vim/bundle/
" Install vundle:
" "git clone https://github.com/VundleVim/Vundle.vim.git " ~/.vim/bundle/Vundle.vim
"
Plugin 'VundleVim/Vundle.vim'


" My plugins:
" Language prediction and completion
"Plugin 'ycm-core/YouCompleteMe'  " for code completion
Plugin 'neoclide/coc.nvim', {'branch': 'release'}  " alternative to YCM
" Enhanced syntax highlighting
Plugin 'sheerun/vim-polyglot'
Plugin 'mechatroner/rainbow_csv' "csv with color
Plugin 'dense-analysis/ale'
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'preservim/nerdtree'
Plugin 'YankRing.vim'
  let g:yankring_history_dir = $HOME.'/.vim/'
  let g:yankring_history_file = '.yankring_history'
Plugin 'kien/ctrlp.vim'
""  Plugin 'sudo.vim'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'mbbill/undotree'
Plugin 'mhinz/vim-signify'
  set updatetime=100
Plugin 'Raimondi/delimitMate'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
  let g:airline_theme="cobalt2"
Plugin 'nathanaelkane/vim-indent-guides'
  let g:indent_guides_auto_colors = 0
  autocmd VimEnter,Colorscheme tir_black :hi IndentGuidesOdd  guibg=red   ctermbg=3
  autocmd VimEnter,Colorscheme tir_black :hi IndentGuidesEven guibg=green ctermbg=4
    set ts=2                " number of spaces in a tab
    set sw=2                " number of spaces for indent
    set et                  " expand tabs into spaces
set list listchars=tab:→\ ,trail:▸
""  Plugin 'hynek/vim-python-pep8-indent'
""    let g:syntastic_python_flake8_args='--ignore=E501'

" JSON
Plugin 'leshill/vim-json'

" HTML
Plugin 'HTML-AutoCloseTag'
Plugin 'hail2u/vim-css3-syntax'
Plugin 'juvenn/mustache.vim'
  let g:mustache_abbreviations = 1

""  Plugin 'zhaocai/GoldenView.Vim'
""  let g:goldenview__enable_default_mapping=0

" All of your Plugins must be added before the following line
" Put your non-Plugin stuff after this line
call vundle#end()            " required
" filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
filetype plugin on
filetype on                  " required off? set to ON 27 02 24
filetype indent on					" enable filetype-specific plugins 27 02 24
colo slate
set encoding=utf-8
set binary

" presentation settings
set number              " precede each line with its line number
set relativenumber      " show relative number
set numberwidth=3       " number of culumns for line numbers
set textwidth=0         " Do not wrap words (insert)
set formatoptions-=awtln  " is tcq
" set nowrap              " Do not wrap words (view)
set wrap
set wrapmargin=0
set showcmd             " Show (partial) command in status line.
set showmatch           " Show matching brackets.
set ruler               " line and column number of the cursor position
set wildmenu            " enhanced command completion
set visualbell          " use visual bell instead of beeping
set noerrorbells				" turn off audio bell
set laststatus=2        " always show the status lines
set list listchars=tab:→\ ,trail:▸
set cursorline				  " hightlight current line

" highlight spell errors
hi SpellErrors guibg=red guifg=black ctermbg=red ctermfg=black
" toggle spell check with F7
map <F7> :setlocal spell! spell?<CR>

" behavior
" ignore these files when completing names and in explorer
set wildignore=.svn,CVS,.git,.hg,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif
set shell=$SHELL        " use current shell for shell commands
set hidden              " enable multiple modified buffers
set history=5000
set autoread            " automatically read file that has been changed on disk and doesn't have changes in vim
set backspace=indent,eol,start
set guioptions-=T       " disable toolbar"
set autoindent smartindent   " automatically indent new line
set completeopt=menuone,preview
set cinoptions=:0,(s,u0,U1,g0,t0 " some indentation options ':h cinoptions' for details
set modelines=5         " number of lines to check for vim: directives at the start/end of file
set lazyredraw          " Don't redraw screen when running macros
set expandtab
set tabstop=2
set shiftwidth=2

" map all the things
let mapleader = ","
let maplocalleader = "\\"
" map jj <esc>
" highlight spell errors
hi SpellErrors guibg=red guifg=black ctermbg=red ctermfg=black
" toggle spell check with F7
map <F7> :setlocal spell! spell?<CR>
map <Leader>p :set paste<CR><esc>"*]p:set nopaste<cr>
map <Leader>t :NERDTreeToggle<CR>

" mouse settings
" if has("mouse")
"   set mouse=c
" endif
set mousehide           " Hide mouse pointer on insert mode."

" search settings
set incsearch           " Incremental search
set hlsearch            " Highlight search match
set ignorecase          " Do case insensitive matching
set smartcase           " do not ignore if search pattern has CAPS

" directory settings
set nobackup            " do not write backup files
set noswapfile          " do not write .swp files
silent !mkdir -vp ~/.backup/vim/undo/ > /dev/null 2>&1
set backupdir=~/.backup/vim,.       " list of directories for the backup file
set directory=~/.backup/vim,~/tmp,. " list of directory names for the swap file
set undofile
set undodir=~/.backup/vim/undo/,~/tmp,.

" folding
set foldcolumn=0        " columns for folding
set foldmethod=indent
set foldlevel=9
set nofoldenable        "dont fold by default "

" Remember last position in file
autocmd BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line("$") |
  \   exe "normal g`\"" |
  \ endif

"setlocal fo+=aw
set colorcolumn=80
set history=10000
syntax on

" FROM C
" Language prediction settings (if using YouCompleteMe)
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_auto_trigger = 1

" Enhanced spell check settings (add to your existing spell settings)
set spell spelllang=en_us
set spellfile=~/.vim/spell/en.utf-8.add
set complete+=kspell
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red

" ALE (Asynchronous Lint Engine) settings
let g:ale_linters = {
\   'python': ['flake8', 'pylint'],
\   'javascript': ['eslint'],
\   'typescript': ['tsserver', 'eslint'],
\   'go': ['gopls'],
\}
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['black'],
\   'javascript': ['prettier'],
\   'typescript': ['prettier'],
\}
let g:ale_fix_on_save = 1

" Code navigation
nnoremap <leader>gd :YcmCompleter GoTo<CR>
nnoremap <leader>gr :YcmCompleter GoToReferences<CR>
nnoremap <leader>gi :YcmCompleter GoToImplementation<CR>
nnoremap <leader>gy :YcmCompleter GoToType<CR>

" Quick spell check navigation
nnoremap <leader>s ]s
nnoremap <leader>S [s
nnoremap <leader>a zg
nnoremap <leader>z z=
