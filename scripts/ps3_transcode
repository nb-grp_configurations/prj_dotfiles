#!/bin/bash

####
#
# ps3_transcode.sh
# -------------------
# A small script to transcode a video file into ps3 compatible 
# format.
#
# This program relies on video format support of ffmpeg. Any file
# that is undestood by ffmpeg can be transcoded using this script.
#
# The script was written because I did not find a decent solution 
# readily available for linux to do the video transcoding for my ps3.
# There were several options for Windows, but those are not too
# useful on linux platform.
#
# Easy example way to transcode all .avi files in $DIR:
#   find $DIR -name "*.avi" | \
#     xargs -I {} echo "ps3_transcode.sh \
#       -input \"{}\" \
#       -output \"{}.mp4\"" >transcode_script.sh
#   chmod +x ./transcode_script.sh
#   ./transcode_script.sh
# 
# Comments warmly welcome to the author:
#
#   Jarkko Vatjus-Anttila <j.vatjus-anttila@luukku.com>
#
VERSION="1.0.1 (25th Jan 2008)"
#
# Version history:
#  v1.0.0 (12th Nov 2007)
#     - A baseline for future development. All basic functions
#       seem to work
#  v1.0.1 (25th Jan 2008)
#     - Added error checking for streams that do not report
#	resoluton and/or FPS correctly.
#     - Added -fps, -xres and -yres options to override the
#	detected values
#


####
# Global variables for the whole script
#
# RATIO:
# ------
# bitrate ratio (values taken from good quality movie samples)
#  1920x1080 -> 8096kbit/s -> 0.00390 kbit/pixel
#   512x384  -> 300kbit/s  -> 0.00153 kbit/pixel
#
# -> choosing ratio: 0.00240 kbits/pixel
#
# This value is used later in this script to quess a good
# bitrate for the video, based on its resolution
#
RATIO=0.00240



####
# CheckDependency() sub routine is used to check external dependencies 
# (commands) that should exist in the system.
#

CheckDependency() {
    if [ -z `which "$1"` ]; then
	echo "$1 does not exist. Maybe \"sudo apt-get install $1\" helps?"
	exit
    fi
    echo "Checking $1 ... ok"
}

####
# Usage() sub routine display the generic usage of the script
#

usage() {
  echo "ps3_transcode.sh -input <file> -output <file> [-subtitles <file>]"
  echo "   [-bitrate <forced bitrate>] [-ratio <value>] [-timelimit <value>]"
  echo "   [-help|--help]"
  echo "Params:"
  echo "  -input        Input file name (e.g. Input.avi)"
  echo "  -output       Output file name (e.g. Output.mp4"
  echo "  -subtitles    Subtitle filename (e.g. Input.srt)"
  echo "  -bitrate      Forced bitrate in kbits/s"
  echo "  -ratio        Compression ratio bits/pixel (default: $RATIO)"
  echo "  -timelimit    Limit transcoding time to N seconds (useful for testing)"
  echo "  -keeptemp     Do not delete temporary coding files (*.aac, *.256)"
  echo "  -fps          Force FPS value (e.g. -fps 25)"
  echo "  -xres         Force X-resolution for input video (e.g. -xres 1920)"
  echo "  -yres         Force Y-resolution for input video (e.g. -yres 1080)"
  echo "  -nice         Nice process value (default 15)"
  echo "  -version      Display version and exit"
  echo "  -help|--help  Display this usage text"
  echo "Example: (transcode one video stream)"
  echo "  ps3_transcode.sh -input video.avi -output video.mp4"
  echo "Example: (transcode all *.avi files in \$DIR)"
  echo "  find \$DIR -name \"*.avi\" | \\"
  echo "    xargs -I {} echo \"ps3_transcode.sh \\"
  echo "    -input \\\"{}\\\" \\"
  echo "    -output \\\"{}.mp4\\\"\" >transcode_script.sh"
  echo "  chmod +x ./transcode_script.sh"
  echo "  ./transcode_script.sh"
}


#### 
# Check input parameter existense
#
INPUT=
OUTPUT=
SUBTITLES=
FORCED_BITRATE=
STOP_FFMPEG=
STOP_MPLAYER=
KEEPTEMP=
NICE=
FORCE_FPS=
FORCE_XRES=
FORCE_YRES=

while [ $# -gt 0 ]; do
	case "$1" in
	-input)
		if [ -z "$2" ]; then
			echo "ERROR: Missing -input argument"
			usage
			exit 1
		fi
		INPUT="$2"
		if [ ! -e "$INPUT" ]; then
			echo "ERROR: Input file does not exist!"
			usage
			exit 1
		fi
		shift;;
	-output)
		if [ -z "$2" ]; then
			echo "ERROR: Missing -output argument"
			usage
			exit 1
		fi
		OUTPUT="$2"
		shift;;
	-subtitles)
		if [ -z "$2" ]; then
			echo "ERROR: Missing -subtitles argument"
			usage
			exit 1
		fi
		SUBTITLES="$2"
		SUBTITLES_CMD="-add $SUBTITLES"
		if [ ! -e "$SUBTITLES" ]; then
			echo "ERROR: Subtitles file does not exist!"
			usage
			exit 1
		fi
		shift;;
	-bitrate)
		if [ -z "$2" ]; then
			echo "ERROR: Missing -bitrate argument"
			usage
			exit 1
		fi
		FORCE_BITRATE="$2"
		echo "bitrate: $FORCE_BITRATE"
		shift;;
	-ratio)
		if [ -z "$2" ]; then
			echo "ERROR: Missing -ratio argument"
			usage
			exit 1
		fi
		RATIO=$2
		shift;;
	-timelimit)
		if [ -z "$2" ]; then
			echo "ERROR: Missing -timelimit argument"
			usage
			exit 1
		fi
		STOP_FFMPEG="-t $2"
		STOP_MPLAYER="-endpos $2"
		shift;;
	-keeptemp)
		KEEPTEMP=1;;
	-nice)
		NICE="$2"
		shift;;
	-version)
		echo "Script Version: $VERSION"
		exit 1;;
	-fps)
		FORCE_FPS=$2
		echo "Forcing FPS: $FORCE_FPS"
		shift;;
	-xres)
		FORCE_XRES=$2
		echo "Forcing X-res: $FORCE_XRES"
		shift;;
	-yres)
		FORCE_YRES=$2
		echo "Forcing Y-res: $FORCE_YRES"
		shift;;
	-help|--help)
		usage
		exit 1;;
	*)
		echo "Unknown option $1"
		usage
		exit 1
	esac
	shift
done

if [ -z "$INPUT" ]; then
	echo "ERROR: No input filename given!"
	usage
	exit 1
fi
if [ -z "$OUTPUT" ]; then
	echo "ERROR: No output filename given!"
	usage
	exit 1
fi

echo "Using input params:"
echo "Input:          $INPUT"
echo "Output:         $OUTPUT"
if [ ! -z "$SUBTITLES" ]; then
	echo "Subtitles:      $SUBTITLES"
fi
if [ ! -z "$FORCE_BITRATE" ]; then
	echo "Forced bitrate: $FORCE_BITRATE"
fi
if [ ! -z "$STOP_FFMPEG" ]; then
	echo "ffmpeg timelimit option \"$STOP_FFMPEG\" set"
fi

if [ "$INPUT" = "$OUTPUT" ]; then
	echo "Input and output are the same. You should not do that!"
	exit 0
fi
if [ -z $NICE ]; then
	NICE=15
fi
echo "Using Nive value: $NICE"


####
# Check Dependencies:
#  - mplayer
#  - faac
#  - ffmpeg
#  - bc
#  - MP4Box
#  - x264
CheckDependency mplayer
CheckDependency ffmpeg
CheckDependency bc
CheckDependency MP4Box
CheckDependency faac
CheckDependency x264



####
# First collect information about the video using mplayer
# x-resolution, y-resolution and original fps
#
VINFO=`mplayer "$INPUT" -endpos 0 -ao null -vo null 2>/dev/null |grep "VIDEO:"`
XRES=`echo $VINFO | cut -f 3 -d " " | cut -f 1 -d "x"`
YRES=`echo $VINFO | cut -f 3 -d " " | cut -f 2 -d "x"`
FPS=`echo $VINFO | cut -f 5 -d " "`

if [ ! -z $FORCE_FPS ]; then
	FPS=$FORCE_FPS
fi
if [ ! -z $FORCE_XRES ]; then
	XRES=$FORCE_XRES
fi
if [ ! -z $FORCE_YRES ]; then
	YRES=$FORCE_YRES
fi

echo "xres=$XRES, yres=$YRES, fps=$FPS"

if [ -z $XRES ]; then
	echo "ERROR: Unable to detect resolution! Abort!"
	exit 0
fi
if [ -z $YRES ]; then
	echo "ERROR: Unable to detect resolution! Abort!"
	exit 0
fi
if [ -z $FPS ]; then
	echo "ERROR: Unable to detect FPS! Abort!"
	exit 0
fi

#exit 0

####
# Calculate good estimation for the bitrate of the video
# using defined RATIO
#
BITRATE=`echo "$XRES * $YRES * $RATIO" | bc | cut -f 1 -d "."`
echo $BITRATE
if [ ! -z $FORCE_BITRATE ]; then
	BITRATE=$FORCE_BITRATE
	echo "Overriding bitrate: $BITRATE"
fi



####
# Audio encoding
#
FAAC_OPTS=" \
  -P \
  -R 44100 \
  -B 16 \
  -C 2 \
  -c 44100 \
  -q 120"

echo "$0: Starting audioencoding"

rm -f "$OUTPUT".aac
nice -n $NICE ffmpeg $STOP_FFMPEG -i "$INPUT" -ar 44100 -f s16be - | \
  nice -n $NICE faac $FAAC_OPTS -o "$OUTPUT".aac -

# This hack is here in place if ffmpeg refuses to recognize audio
# stream format. This is bad code, but at the moment best that I can 
# think of. We use mplayer to decode audio into pcm, transcode using ffmpeg
# to 44.1KHz sampling rate and encode it afterwards with faac.
if [ -z "`cat "$OUTPUT".aac`" ]; then
	echo "OMG aac file empty!"
	rm -f "$OUTPUT".aac
	nice -n $NICE mplayer -quiet -vo null -vc null $STOP_MPLAYER \
	  -ao pcm:waveheader:file="$OUTPUT".2.wav "$INPUT"
	nice -n $NICE ffmpeg $STOP_FFMPEG -i "$OUTPUT".2.wav -ar 44100 "$OUTPUT".wav
	nice -n $NICE faac "$OUTPUT".wav -o "$OUTPUT".aac
#	rm -f "$OUTPUT"*.wav
	exit 1
fi


####
# Video encoding
#
X264_OPTS=" \
  --crf 20.0 \
  --sar 1:1 \
  --partitions all \
  --aud \
  --level 4.1 \
  --ref 3 \
  --mixed-refs \
  --bframes 3 \
  --b-pyramid \
  --direct auto \
  --direct-8x8 1 \
  --8x8dct \
  --analyse all \
  --subme 6 \
  --me umh \
  --b-rdo \
  --bime \
  --weightb \
  --trellis 1 \
  --pre-scenecut \
  --threads auto \
  --no-ssim \
  --no-psnr \
  --progress"

echo "$0: Starting video encoding"

rm -f "$OUTPUT".264
nice -n $NICE ffmpeg $STOP_FFMPEG -i "$INPUT" -f rawvideo - | \
  nice -n $NICE x264 $X264_OPTS -o "$OUTPUT".264 - ${XRES}x${YRES}

#if [ -z "`cat "$OUTPUT".264`" ]; then
#	echo "OMG 264 file empty!"
#	exit 0
#fi



####
# A/V multiplexing
# 
echo "$0: Starting MP4Box multiplexing"
MP4Box -new \
  -add "$OUTPUT".aac \
  -add "$OUTPUT".264 \
  $SUBTITLES_CMD \
  -fps $FPS  \
  "$OUTPUT"



####
# Cleanup
#
if [ -z $KEEPTEMP ]; then
	rm -f "$OUTPUT".aac "$OUTPUT".264
fi



####
# End
#
echo ""
echo "Done! Result in $OUTPUT"

