#!/usr/bin/tclsh
#
# work/lunch/meeting status 
#
# written by nathan black 
#
# Version 0.9 updated status replies to include the eod status
# Version 0.8 updated status replies to include the break status
# Version 0.7 Initial release
#
# Description: provides a simple status for teammates
#
# usage: !s !state <status> where <status> is one of working, eod, break, lunch, project, meeting, vacation
#        ?s, ?stat or ?status to retrive the current status list
#
#
#
#

bind pub - !status proc_state
bind pub - !s proc_state
proc proc_state {ircnick userhost handle channel arg} {
   set status [string tolower "$arg"]
   putlog "query from $ircnick $channel $arg"
   if {[lsearch {"outing" "break" "eod" "eto" "working" "lunch" "project" "meeting"} $status] == -1}  {
     puthelp "PRIVMSG $ircnick :Please supply your current state. Accepted states are break, eod, eto, working, lunch, project or meeting. To list status use ?status or ?s, to set status use !status <status> or !s <status> Use curl -s http://focus.digitalboy.net/status.txt to dump the list." 
     return 1
   } else {

      set fs  [open "/srv/httpd/htdocs/status.txt" r]
      set all_lines [split [read $fs] "\n"]
      close $fs

      set write_data [open "/srv/httpd/htdocs/status.txt" w]
      foreach file_line $all_lines {
         if {$file_line == ""} { continue }

         lassign $file_line utime nick state id
         if {$nick == $ircnick} {
            puts $write_data "[clock format [clock seconds] -format "%d%b%H:%M"] $nick $status $id"
	    puthelp "PRIVMSG $ircnick : Thank you your status has been updated to $status"
         } {
            puts $write_data $file_line
         }
      }
      close $write_data
   }
   return 0
}


bind pub - ?status proc_post
bind pub - ?stat proc_post
bind pub - ?s proc_post
proc proc_post { ircnick userhost handle channel arg } {
   set fs [open "/srv/httpd/htdocs/status.txt" r]
   putlog "status query from $ircnick $channel $arg"
   set data [read $fs]
   close $fs
   set lines [split $data "\n"]
   putmsg $ircnick $lines
}

bind pub - !back proc_back
proc proc_back {ircnick userhost handle channel arg} {
     puthelp "PRIVMSG $ircnick :Whoops. !back is deprecated.  When setting status please use !status <status> or !s <status> . Use curl -s http://focus.digitalboy.net/status.txt to dump the list." 
}

bind pub - !lunch proc_lunch
proc proc_lunch {ircnick userhost handle channel arg} {
     puthelp "PRIVMSG $ircnick :Whoops. !lunch is deprectated. When setting status please use !status <status> or !s <status> . Use curl -s http://focus.digitalboy.net/status.txt to dump the list." 
}

setudef flag nickdetector
bind nick - * changenick
proc changenick {ircnick uhost hand chan newnick} {
   putlog "nick query ircnick $ircnick hand $hand chan $chan newnick $newnick"
      set fs  [open "/srv/httpd/htdocs/status.txt" r]
      set all_lines [split [read $fs] "\n"]
      close $fs

      set write_data [open "/srv/httpd/htdocs/status.txt" w]
      foreach file_line $all_lines {
         if {$file_line == ""} { continue }

         lassign $file_line utime nick state id
         if {$nick == $ircnick} {
            puts $write_data "$utime $newnick $state $id"
	    puthelp "PRIVMSG $newnick : Thank you your nick has been updated to $newnick"
         } {
            puts $write_data $file_line
         }
      }
      close $write_data
   }
   return 0
}

# op routine template
#setudef flag opoverride
#bind nick o * changenick
#proc changenick {ircnick uhost hand chan newnick} {
#   putlog "nick query ircnick $ircnick hand $hand chan $chan newnick $newnick"
#      set fs  [open "/srv/httpd/htdocs/status.txt" r]
#      set all_lines [split [read $fs] "\n"]
#      close $fs
#
#      set write_data [open "/srv/httpd/htdocs/status.txt" w]
#      foreach file_line $all_lines {
#         if {$file_line == ""} { continue }
#
#         lassign $file_line utime nick state id
#         if {$nick == $ircnick} {
#            puts $write_data "$utime $newnick $state $id"
#	    puthelp "PRIVMSG $newnick : Thank you your nick has been updated to $newnick"
#         } {
#            puts $write_data $file_line
#         }
#      }
#      close $write_data
#   }
#   return 0
#}
